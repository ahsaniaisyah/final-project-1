<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Profile;
use Illuminate\Support\Facades\Auth;

class ProfileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //make user_id => name from profile
        $data_profile = Profile::all();
        $newCollection = $data_profile->mapWithKeys(function ($item) {
            return [
                $item['user_id'] => [
                    'id' => $item['id'],
                    'name' => $item['name'],
                    'image_profile' => $item['image_profile'],
                ]
            ];
        });

        $newCollection->all();
        // dd($newCollection[12]['name']);


        $profile_data = Profile::where('user_id', Auth::id())->first();
        $user_info = Auth::user()->id;
        if ($profile_data) {
            $profile = $profile_data;
        } else {
            $profile = (object) array("name" => null, "location" => null, "bio" => null, "image_profile" => "no_image_profile.png", "id" => null);
        }
        $following = DB::table('follow')->where('follower_id', Auth::id());
        $jml_following = $following->count();
        $data_following = $following->get();
        $follower = DB::table('follow')->where('user_id', Auth::id());
        $jml_follower = $follower->count();
        $data_follower = $follower->get();

        return view('profile.index', compact('profile', 'user_info', 'jml_following', 'jml_follower', 'data_following', 'data_follower', 'newCollection'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //$profile = DB::table('profile')->get();
        // return view ('profile.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (Auth::user()->profile) {
            $profile_id = Auth::user()->profile->id;
        } else {
            $profile_id = 0;
        }
        // dd($profile_id);
        $request->validate([
            'profile_id' => 'exists:profile,id',
            'name' => 'required|alpha_dash|unique:profile,name,' . $profile_id,
            'location' => 'required',
            'bio' => 'required',
            'user_id' => 'required|exists:users,id',
            'image_profile' => 'required_without:profile_id|image|mimes:jpeg,png,jpg|max:2048',
        ]);

        if ($request->profile_id) {
            $profile = Profile::find($request->profile_id);
            if ($request->image_profile) {
                $image_path = public_path('images') . '/' . $profile->image_profile;
                unlink($image_path);

                $image_profileName = time() . "." . $request->image_profile->extension();
                $request->image_profile->move(public_path('images'), $image_profileName);
            } else {
                $image_profileName = $profile->image_profile;
            }
        } else {
            $image_profileName = time() . "." . $request->image_profile->extension();
            $request->image_profile->move(public_path('images'), $image_profileName);
        }

        Profile::updateOrCreate(
            [
                'id' => $request->profile_id
            ],
            array_merge(
                [
                    "name" => $request["name"]
                ],
                [
                    "location" => $request["location"]
                ],
                [
                    "bio" => $request["bio"]
                ],
                [
                    "user_id" => $request["user_id"]
                ],
                [
                    "image_profile" => $image_profileName
                ],
            )
        );
        return redirect('/profile');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // dd("show profile that id = " . $id);
        $profile = Profile::where('id', $id)->first();

        //jika sudah terfollow ubah tombol di profile.show jadi followed
        $follow_already = DB::table('follow')->where('user_id', $profile->user_id)->where('follower_id', Auth::id())->first();
        if ($follow_already) {
            $followed = true;
        } else {
            $followed = false;
        }
        $user_info = Auth::id();

        $following = DB::table('follow')->where('follower_id', $profile->user_id);
        $jml_following = $following->count();
        $data_following = $following->get();
        $follower = DB::table('follow')->where('user_id', $profile->user_id);
        $jml_follower = $follower->count();
        $data_follower = $follower->get();

        return view('profile.show', compact('profile', 'user_info', 'followed', 'jml_following', 'jml_follower', 'data_following', 'data_follower'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        // $profile = DB::table('profile')->where('id', $id)->first();
        // return view('profile.edit', compact('profile'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    // public function update(Request $request, $id)
    // {
    //     $request->validate([
    //         'location' => 'required',
    //         'bio' => 'required',
    //     ]);
    //     $query = DB::table('profile')
    //         ->where('id', $id)
    //         ->update([
    //             'location' => $request['location'],
    //             'bio' => $request['bio'],
    //         ]);

    //     return redirect('/profile');
    // }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    // public function destroy($id)
    // {
    //     DB::table('profile')->where('id', $id)->delete();
    //     return redirect('/profile');
    // }
}
