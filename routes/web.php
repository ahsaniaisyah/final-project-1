<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;

Auth::routes();

Route::group(['middleware' => ['auth']], function () {
    Route::get('/', 'indexController@home')->name('home');

    Route::resource('profile', 'ProfileController');
    Route::resource('follow', 'FollowController');

    Route::resource('post', 'PostController');
});

//Route Resource example
// Route::get('/example', 'ExampleController@index')->name('example.index');
// Route::get('/example/create', 'ExampleController@create')->name('example.create');
// Route::post('/example', 'ExampleController@store')->name('example.store');
// Route::get('/example/{example_id}', 'ExampleController@show')->name('example.show');
// Route::get('/example/{example_id}/edit', 'ExampleController@edit')->name('example.edit');
// Route::put('/example/{example_id}', 'ExampleController@update')->name('example.update');
// Route::delete('/example/{example_id}', 'ExampleController@destroy')->name('example.destroy');


// Route::get('/', function () {
//     return view('welcome');
// });

// Route::get('/master', function () {
//     return view('layout.master');
// });



// Route::get('/show', function () {
//     return view('profile.show');
// });

// Route::get('/edit', function () {
//     return view('profile.edit');
// });



//dikomen krn sudah pakai route bawaan Auth
// Route::get('/register', function () {
//     return view('pages.register');
// });

// Route::get('/login', function () {
//     return view('pages.login');
// });