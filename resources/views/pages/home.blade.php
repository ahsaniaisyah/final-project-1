@extends('layout.master')

@section('title')
<p>Home</p>
@endsection

@section('content')

<!-- di halaman Home ada button buat create, edit, delete postingan sendiri. postingan2 yang udah dibikin ditampilin juga di halaman ini -->


@if($post === 1)
@php
$modal_id = "modalPost";
$modal_target = "#modalPost";
@endphp
@else
@php
$modal_id = "postModal";
$modal_target = "#postModal";
@endphp
@endif
<div class="col-lg-8 col-xl-9">
    <button class="btn btn-primary mb-5" data-toggle="modal" data-target="{{$modal_target}}">Make a new post</button>

    <!-- Modal Post-->
    <div class="modal fade" id="{{$modal_id}}" tabindex="-1" role="dialog" aria-labelledby="postModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="postModalLabel">Make a new post</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form class="form-valide" action="{{route('post.store')}}" method="post" enctype="multipart/form-data">
                        @csrf
                        <input type="text" class="form-control input-default" id="user_info" name="user_info" value="{{ $user_info}}" hidden>
                        <div class="row">
                            <div class="col">
                                <div class="form-group row">
                                    <label class="col-lg-4 col-form-label" for="location">Pesan <span class="text-danger">*</span>
                                    </label>
                                    <textarea class="form-control" name="pesan" id="pesan" cols="30" rows="2" placeholder="Post a new message"></textarea>
                                    @error('pesan')
                                    <div class="alert alert-danger">
                                        {{ $message }}
                                    </div>
                                    @enderror
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col">
                                <div class="form-group row">
                                    <label class="col-lg-4 col-form-label" for="location">Gambar <span class="text-danger">*</span>
                                    </label>
                                    <input type="file" class="form-control input-default" id="gambar" name="gambar">
                                    @error('gambar')
                                    <div class="alert alert-danger">
                                        {{ $message }}
                                    </div>
                                    @enderror
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col">
                                <div class="form-group row">
                                    <label class="col-lg-4 col-form-label" for="location">Caption <span class="text-danger">*</span>
                                    </label>
                                    <input type="text" class="form-control input-default" id="caption" name="caption" placeholder="Enter a caption.." value="{{old('caption')}}">
                                    @error('caption')
                                    <div class="alert alert-danger">
                                        {{ $message }}
                                    </div>
                                    @enderror
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col">
                                <div class="form-group row">
                                    <label class="col-lg-4 col-form-label" for="location">Quote <span class="text-danger">*</span>
                                    </label>
                                    <input type="text" class="form-control input-default" id="quote" name="quote" placeholder="Enter a quote.." value="{{old('quote')}}">
                                    @error('quote')
                                    <div class="alert alert-danger">
                                        {{ $message }}
                                    </div>
                                    @enderror
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12 text-center">
                                <button type="submit" class="btn btn-primary block">Send</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>



    <div class="card">
        <div class="card-body">
            @foreach ($all_post as $data)
            <div class="media media-reply">
                <img class="mr-3 circle-rounded" src="{{asset('images/'.$newCollection[$data->user_id]['image_profile'])}}" width="50" height="50" alt="Generic placeholder image">
                <div class="media-body">
                    <div class="d-sm-flex justify-content-between mb-2">
                        <h5 class="mb-sm-0">{{$newCollection[$data->user_id]['name']}} <small class="text-muted ml-3">{{$data->created_at}}</small></h5>
                        <div class="media-reply__link">
                            <button class="btn btn-transparent p-0 mr-3"><i class="fa fa-thumbs-up"></i></button>
                            <button class="btn btn-transparent p-0 mr-3"><i class="fa fa-thumbs-down"></i></button>
                            <button class="btn btn-transparent p-0 ml-3 font-weight-bold">Reply</button>
                        </div>
                    </div>

                    <p>{{$data->pesan}}</p>
                    <p> <img class="img-fluid" src="{{asset('images/'.$data->gambar)}}" alt="Generic placeholder image"></p>
                </div>
            </div>
            @endforeach
        </div>
    </div>
</div>

@endsection