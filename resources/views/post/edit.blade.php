@extends('layout.master')

@section('title')
<p>Home</p>
@endsection

@section('content')

<!-- di halaman Home ada button buat create, edit, delete postingan sendiri. postingan2 yang udah dibikin ditampilin juga di halaman ini -->

<div class="card">
    <div class="card-body">
        <form action="/post/{{$post->id}}" method="post" enctype="multipart/form-data" class="form-profile">
            @csrf
            @method('put')
            <div class="form-group">
                <textarea name="tulisan" value={{$post->tulisan}} class="form-control"  id="" cols="15" rows="2" placeholder="Post a new message"></textarea>
            
            <div class="d-flex align-items-center">
                <ul class="mb-0 form-profile__icons">
                    
                    
                    <div class="form-group">
                    <li class="d-inline-block">
                        <button class="btn btn-transparent p-0 mr-3"><input type="file" value={{$post->gambar}} name="gambar" class="form-control"></i></button>
                    </li>
                    </div>
                    
                </ul>
                <button class="btn btn-primary px-3 ml-4">Send</button>
            </div>
        </form>
    </div>
</div>


@endsection
