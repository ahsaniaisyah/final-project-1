 <!--**********************************
            Nav header start
        ***********************************-->
 <div class="nav-header">
     <div class="brand-logo">
         <a href="route('home)">
             <b class="logo-abbr"><img src="{{asset('quixlab-master/images/s_trans.png')}}" alt="ccc"> </b>
             <span class="logo-compact"><img src="{{asset('quixlab-master/images/s_trans.png')}}" alt="aaa"></span>
             <span class="brand-title">
                 <img src="{{asset('quixlab-master/images/sosmedku_trans.png')}}" alt="ddd" class="img-thumbnail" style="background-color:transparent">
             </span>
         </a>
     </div>
 </div>
 <!--**********************************
            Nav header end
        ***********************************-->

 <!--**********************************
            Header start
        ***********************************-->
 <div class="header">
     <div class="header-content clearfix">

         <div class="nav-control">
             <div class="hamburger">
                 <span class="toggle-icon"><i class="icon-menu"></i></span>
             </div>
         </div>
         <div class="header-left">
             <div class="row mt-4">
                 <div class="col">
                     <select class="form-control" id="su" style="width: 200%;">
                         <option></option>
                         @foreach ($show_all_users_everywhere as $data)
                         <option value="{{route('profile.show',$data->id)}}">{{$data->name}}</option>
                         @endforeach
                     </select>
                 </div>
             </div>
         </div>

         <div class="header-right">
             <ul class="clearfix">
                 <li class="icons dropdown"><a href="javascript:void(0)" data-toggle="dropdown">
                         <i class="mdi mdi-email-outline"></i>
                         <span class="badge gradient-1 badge-pill badge-primary">3</span>
                     </a>
                     <div class="drop-down animated fadeIn dropdown-menu">
                         <div class="dropdown-content-heading d-flex justify-content-between">
                             <span class="">3 New Messages</span>

                         </div>
                     </div>

                 <li class="icons dropdown"><a href="javascript:void(0)" data-toggle="dropdown">
                         <i class="mdi mdi-bell-outline"></i>
                         <span class="badge badge-pill gradient-2 badge-primary">2</span>
                     </a>
                     <div class="drop-down animated fadeIn dropdown-menu dropdown-notfication">
                         <div class="dropdown-content-heading d-flex justify-content-between">
                             <span class="">2 New Notifications</span>

                         </div>
                     </div>
                 </li>

                 <li class="icons dropdown">
                     <div class="user-img c-pointer position-relative" data-toggle="dropdown">
                         <span class="activity active"></span>
                         @auth
                         @isset (auth()->user()->profile->image_profile)
                         <img src="{{asset('images/'.auth()->user()->profile->image_profile)}}" height="40" width="40" alt="">
                         @else
                         <img src="{{asset('images/no_image_profile.png')}}" height="40" width="40" alt="">
                         @endisset
                         @endauth
                         @guest
                         <img src="{{asset('images/guest.png')}}" height="40" width="40" alt="">
                         @endguest
                     </div>
                     <div class="drop-down dropdown-profile dropdown-menu">
                         <div class="dropdown-content-body">
                             <ul>
                                 <li>
                                     <a href="{{route('profile.index')}}"><i class="icon-user"></i> <span>Profile</span></a>
                                 </li>
                                 <hr class="my-2">
                                 @guest
                                 <li>
                                     <a href="{{route('login')}}"><i class="icon-key"></i> <span>Login</span></a>
                                 </li>
                                 @endguest
                                 @auth
                                 <li>
                                     <a href="{{ route('logout') }}" onclick="event.preventDefault();
                document.getElementById('logout-form').submit();">
                                         <i class="icon-logout"></i>
                                         <span>
                                             Logout
                                         </span>
                                     </a>
                                     <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                         @csrf
                                     </form>
                                 </li>
                                 @endauth
                             </ul>
                         </div>
                     </div>
                 </li>
             </ul>
         </div>
     </div>
 </div>
 <!--**********************************
            Header end ti-comment-alt
        ***********************************-->