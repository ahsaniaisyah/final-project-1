<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Profile;
use App\Post;

class indexController extends Controller
{
    public function home()
    {
        $post = 0;
        $user_info = Auth::id();
        //profil data from user_id
        $data_profile = Profile::all();
        $newCollection = $data_profile->mapWithKeys(function ($item) {
            return [
                $item['user_id'] => [
                    'id' => $item['id'],
                    'name' => $item['name'],
                    'image_profile' => $item['image_profile'],
                ]
            ];
        });
        $newCollection->all();

        $all_post = Post::orderBy('id', 'DESC')->get();
        // dd($all_post);


        return view('pages.home', compact('user_info', 'post', 'all_post', 'newCollection'));
    }
}
