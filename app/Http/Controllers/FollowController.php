<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Follow;
use Illuminate\Support\Facades\Auth;

class FollowController extends Controller
{
    public function index()
    {
        //     $follow_data = Follow::where('user_id', Auth::id())->first();
        //     $user_info = Auth::user()->id;
        //     if ($follow_data) {
        //         $follow = $follow_data;
        //     } else {
        //         $follow = (object) array("name" => null, "location" => null, "bio" => null, "image_follow" => "no_image_follow.png", "id" => null);
        //     }
        //     // dd($user_info);
        //     return view('follow.index', compact('follow', 'user_info'));
    }

    public function store(Request $request)
    {
        // dd($request->all());
        $request->validate([
            'user_id' => 'required|exists:users,id',
            'follower_id' => 'required|exists:users,id',
        ]);

        $profile_id = $request["profile_id"];
        $exist_follow = Follow::where('user_id', $request->user_id)->where('follower_id', $request->follower_id)->first();

        //jika sudah terfollow maka unfollow, jika belum maka follow
        if ($exist_follow) {
            $exist_follow->delete();
        } else {
            Follow::create([
                "user_id" => $request["user_id"],
                "follower_id" => $request["follower_id"],
            ]);
        }
        return redirect('/profile/' . $profile_id);
    }

    public function show($id)
    {
        // $follow = DB::table('follow')->where('id', $id)->first();
        // return view('follow.follow', compact('follow'));
    }

    public function edit($id)
    {
        // $follow = DB::table('follow')->where('id', $id)->first();
        // return view('follow.edit', compact('follow'));
    }

    public function update(Request $request, $id)
    {
        // $request->validate([
        //     'location' => 'required',
        //     'bio' => 'required',
        // ]);
        // $query = DB::table('follow')
        //     ->where('id', $id)
        //     ->update([
        //         'location' => $request['location'],
        //         'bio' => $request['bio'],
        //     ]);

        // return redirect('/follow');
    }

    public function destroy($id)
    {
        // DB::table('follow')->where('id', $id)->delete();
        // return redirect('/follow');
    }
}
