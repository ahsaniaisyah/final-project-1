@extends('layout.master')

@section('title')
<p>Home</p>
@endsection

@section('content')

<!-- di halaman Home ada button buat create, edit, delete postingan sendiri. postingan2 yang udah dibikin ditampilin juga di halaman ini -->


@auth
@include('post.create')

@include('post.index')


@endauth

@guest
<p>You're not logged in</p>
@endguest

@endsection
