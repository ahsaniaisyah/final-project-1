@extends('layout.master')

@section('title')
<p>Profile</p>
@endsection

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-lg-4">
            <div class="card">
                <div class="card-body">
                    <div class="media align-items-center mb-4">
                        <img class="mr-3" src="{{asset('images/'.$profile->image_profile)}}" width="80" height="80" alt="">
                        <div class="media-body">
                            <h3 class="mb-0">{{$profile->name}}</h3>
                            <p class="text-muted mb-0">{{$profile->location}}</p>
                        </div>
                    </div>

                    <div class="row mb-5">
                        <div class="col">
                            <a data-toggle="modal" data-target="#followingModal">
                                <div class="card card-profile text-center">
                                    <span class="mb-1 text-primary"><i class="icon-people"></i></span>
                                    <h3 class="mb-0">{{$jml_following}}</h3>
                                    <p class="text-muted px-4">Following</p>
                                </div>
                            </a>
                        </div>
                        <div class="col">
                            <a data-toggle="modal" data-target="#followerModal">
                                <div class="card card-profile text-center">
                                    <span class="mb-1 text-warning"><i class="icon-user-follow"></i></span>
                                    <h3 class="mb-0">{{$jml_follower}}</h3>
                                    <p class="text-muted">Followers</p>
                                </div>
                            </a>
                        </div>
                    </div>

                    <h4>About Me</h4>
                    <p class="text-muted">{{$profile->bio}}</p>
                    <!-- <ul class="card-profile__info">
                        <li class="mb-1"><strong class="text-dark mr-4">Mobile</strong> <span>01793931609</span></li>
                        <li><strong class="text-dark mr-4">Email</strong> <span>name@domain.com</span></li>
                    </ul> -->
                </div>
            </div>
        </div>
        <div class="col-lg-8">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title mb-5">Edit Profile</h4>
                    <div class="form-validation">
                        <form class="form-valide" action="{{route('profile.store')}}" method="post" enctype="multipart/form-data">
                            @csrf
                            @if ($profile->id != null)
                            <input type="text" class="form-control input-default" id="profile_id" name="profile_id" placeholder="" value="{{$profile->id}}" hidden>
                            @endif
                            <input type="text" class="form-control input-default" id="user_id" name="user_id" placeholder="" value="{{$user_info}}" hidden>
                            <div class="form-group row">
                                <label class="col-lg-4 col-form-label" for="location">Name <span class="text-danger">*</span>
                                </label>
                                <div class="col-lg-8">
                                    <input type="text" class="form-control input-default" id="name" name="name" placeholder="Enter a name.." value="{{old('name', $profile->name)}}">
                                    @error('name')
                                    <div class="alert alert-danger">
                                        {{ $message }}
                                    </div>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-lg-4 col-form-label" for="location">Location <span class="text-danger">*</span>
                                </label>
                                <div class="col-lg-8">
                                    <input type="text" class="form-control input-default" id="location" name="location" placeholder="Enter a location.." value="{{old('location', $profile->location)}}">
                                    @error('location')
                                    <div class="alert alert-danger">
                                        {{ $message }}
                                    </div>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-lg-4 col-form-label" for="bio">Biodata <span class="text-danger">*</span>
                                </label>
                                <div class="col-lg-8">
                                    <textarea class="form-control" id="bio" name="bio" rows="5" placeholder="Please describe about Yourself...">{{old('bio', $profile->bio)}}</textarea>
                                    @error('bio')
                                    <div class="alert alert-danger">
                                        {{ $message }}
                                    </div>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-lg-4 col-form-label" for="image_profile">Image profile <span class="text-danger">*</span>
                                </label>
                                <div class="col-lg-8">
                                    <input type="file" class="form-control-file input-default" id="image_profile" name="image_profile" placeholder="Select a image profile.." accept=".jpg,.jpeg,.png">
                                    @error('image_profile')
                                    <div class="alert alert-danger">
                                        {{ $message }}
                                    </div>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-lg-8 ml-auto">
                                    <button type="submit" class="btn btn-primary">Submit</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- #/ container -->

<!-- Modal Following-->
<div class="modal fade" id="followingModal" tabindex="-1" role="dialog" aria-labelledby="followingModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="followingModalLabel">Following</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <ul class="list-group">
                    @foreach ($data_following as $data)
                    <li class="list-group-item">
                        <img class="rounded-circle" src="{{asset('images/'.$newCollection[$data->user_id]['image_profile'])}}" height="40" width="40" alt="">
                        {{$newCollection[$data->user_id]['name']}}
                        <a href="{{route('profile.show',$newCollection[$data->user_id]['id'])}}" class="float-right" target="_blank"><button class="btn btn-secondary btn-sm">Following</button></a>
                    </li>
                    @endforeach
                </ul>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal Follower-->
<div class="modal fade" id="followerModal" tabindex="-1" role="dialog" aria-labelledby="followerModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="followerModalLabel">Follower</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <ul class="list-group">
                    @foreach ($data_follower as $data)
                    <li class="list-group-item">
                        <img class="rounded-circle" src="{{asset('images/'.$newCollection[$data->follower_id]['image_profile'])}}" height="40" width="40" alt="">
                        {{$newCollection[$data->follower_id]['name']}}
                        <a href="{{route('profile.show',$newCollection[$data->follower_id]['id'])}}" class="float-right" target="_blank"><button class="btn btn-secondary btn-sm">Remove</button></a>
                    </li>
                    @endforeach
                </ul>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

@endsection