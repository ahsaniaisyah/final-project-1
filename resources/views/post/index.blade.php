@extends('layout.master')

@section('title')
<p>Home</p>
@endsection

@section('content')

<div class="card">
    <div class="card-body">
        <div class="media media-reply">
            <img class="mr-3 circle-rounded" src="images/avatar/2.jpg" width="50" height="50" alt="Generic placeholder image">
            <div class="media-body">
                <div class="d-sm-flex justify-content-between mb-2">
                    @if ($post = null)
                    <form action="{{route('post.store')}}" method="POST">
                        <a href="/post/{{$post->id}}" class="btn btn-info btn-sm">Detail</a>
                        <a href="/post/{{$post->id}}/edit" class="btn btn-warning btn-sm">Edit</a>
                            @method('delete')
                            @csrf
                            <input type="submit" class="btn btn-danger btn-sm" value="Delete">
                        </form>
                        @endif
                    <h5 class="mb-sm-0">Milan Gbah </h5>
                    <div class="media-reply__link">
                        <button class="btn btn-transparent p-0 mr-3"><i class="fa fa-thumbs-up"></i></button>
                        <button class="btn btn-transparent p-0 mr-3"><i class="fa fa-thumbs-down"></i></button>
                        <button class="btn btn-transparent text-dark font-weight-bold p-0 ml-2">Reply</button>
                    </div>
                </div>

                <p>Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin. Cras purus odio, vestibulum in vulputate at, tempus viverra turpis. Fusce condimentum nunc ac nisi vulputate fringilla. Donec lacinia congue felis in faucibus.</p>
                <ul>
                    <li class="d-inline-block"><img class="rounded" width="60" height="60" src="images/blog/2.jpg" alt=""></li>
                    <li class="d-inline-block"><img class="rounded" width="60" height="60" src="images/blog/3.jpg" alt=""></li>
                    <li class="d-inline-block"><img class="rounded" width="60" height="60" src="images/blog/4.jpg" alt=""></li>
                    <li class="d-inline-block"><img class="rounded" width="60" height="60" src="images/blog/1.jpg" alt=""></li>
                </ul>

                <div class="media mt-3">
                    <img class="mr-3 circle-rounded circle-rounded" src="images/avatar/4.jpg" width="50" height="50" alt="Generic placeholder image">
                    <div class="media-body">
                        <div class="d-sm-flex justify-content-between mb-2">
                            <h5 class="mb-sm-0">Milan Gbah <small class="text-muted ml-3">about 3 days ago</small></h5>
                            <div class="media-reply__link">
                                <button class="btn btn-transparent p-0 mr-3"><i class="fa fa-thumbs-up"></i></button>
                                <button class="btn btn-transparent p-0 mr-3"><i class="fa fa-thumbs-down"></i></button>
                                <button class="btn btn-transparent p-0 ml-3 font-weight-bold">Reply</button>
                            </div>
                        </div>
                        <p>Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin. Cras purus odio, vestibulum in vulputate at, tempus viverra turpis. Fusce condimentum nunc ac nisi vulputate fringilla. Donec lacinia congue felis in faucibus.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection