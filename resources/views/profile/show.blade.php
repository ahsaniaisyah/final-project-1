@extends('layout.master')

@section('title')
<p>Profile</p>
@endsection

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-lg-6">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title mb-5">Show Profile</h4>
                    <div class="card">
                        <div class="card-body">
                            <div class="media align-items-center mb-4">
                                <img class="mr-3" src="{{asset('images/'.$profile->image_profile)}}" width="80" height="80" alt="">
                                <div class="media-body">
                                    <h3 class="mb-0">{{$profile->name}}</h3>
                                    <p class="text-muted mb-0">{{$profile->location}}</p>
                                </div>
                            </div>
                            <div class="row mb-5">
                                <div class="col">
                                    <div class="card card-profile text-center">
                                        <span class="mb-1 text-primary"><i class="icon-people"></i></span>
                                        <h3 class="mb-0">{{$jml_following}}</h3>
                                        <p class="text-muted px-4">Following</p>
                                    </div>
                                </div>
                                <div class="col">
                                    <div class="card card-profile text-center">
                                        <span class="mb-1 text-warning"><i class="icon-user-follow"></i></span>
                                        <h3 class="mb-0">{{$jml_follower}}</h3>
                                        <p class="text-muted">Followers</p>
                                    </div>
                                </div>
                                <div class="col-12 text-center">
                                    <form class="form-valide" action="{{route('follow.store')}}" method="post">
                                        @csrf
                                        <input type="text" class="form-control input-default" id="profile_id" name="profile_id" placeholder="" value="{{$profile->id}}" hidden>
                                        <input type="text" class="form-control input-default" id="user_id" name="user_id" placeholder="" value="{{$profile->user_id}}" hidden>
                                        <input type="text" class="form-control input-default" id="follower_id" name="follower_id" placeholder="" value="{{$user_info}}" hidden>
                                        @if ($followed === true)
                                        <button type="submit" class="btn btn-secondary px-5" id="followed_button"><i class="fa fa-check"></i> Following</button>
                                        @else
                                        <button type="submit" class="btn btn-danger px-5" id="following_button">Follow Now</button>
                                        @endif
                                    </form>
                                </div>
                            </div>
                            <h4>About Me</h4>
                            <p class="text-muted">{{$profile->bio}}</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection