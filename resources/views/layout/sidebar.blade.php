<div class="nk-sidebar">
    <div class="nk-nav-scroll">

        <ul class="metismenu" id="menu">
            <li>
                <a href="{{ route('profile.index') }}" aria-expanded="false">
                    <span class="user-img c-pointer position-relative">
                        @auth
                        @isset (auth()->user()->profile->image_profile)
                        <img src="{{asset('images/'.auth()->user()->profile->image_profile)}}" height="40" width="40" alt="">
                        <span class="nav-text">{{strtoupper(explode(' ',trim(auth()->user()->profile->name))['0'])}}</span>
                        @else
                        <img src="{{asset('images/no_image_profile.png')}}" height="40" width="40" alt="">
                        <span class="nav-text">{{strtoupper(explode(' ',trim(auth()->user()->name))['0'])}}</span>
                        @endisset
                        @endauth
                        @guest
                        <img src="{{asset('images/guest.png')}}" height="40" width="40" alt="">
                        <span class="nav-text">Guest</span>
                        @endguest
                    </span>
                </a>
            </li>
        </ul>
        <ul class="metismenu" id="menu">
            <li>
                <a href="{{ route('home') }}" aria-expanded="false">
                    <i class="icon-home menu-icon"></i><span class="nav-text">Home</span>
                </a>
            </li>

            @auth
            <li>
                <a href="{{ route('profile.index') }}" aria-expanded="false">
                    <i class="icon-settings menu-icon"></i><span class="nav-text">Profile</span>
                </a>
            </li>
            <li>
                <a class="nav-link" href="{{ route('logout') }}" onclick="event.preventDefault();
                document.getElementById('logout-form').submit();">
                    <i class="icon-logout menu-icon" aria-hidden="true"></i>
                    <span class="nav-text">
                        Logout
                    </span>
                </a>
                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    @csrf
                </form>
            </li>
            @endauth

            @guest
            <li>
                <a href="{{ route('login') }}" aria-expanded="false">
                    <i class="icon-login menu-icon"></i><span class="nav-text">Login</span>
                </a>
            </li>
            @endguest
        </ul>
    </div>
</div>