<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Profile;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
        view()->composer(
            'layout.nav',
            function ($view) {
                $view->with('show_all_users_everywhere', Profile::all());
            }
        );
    }
}
