<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Post;
use App\Profile;
use Illuminate\Support\Facades\Auth;

class PostController extends Controller
{
    public function index()
    {
        //     $post_data = post::where('user_id', Auth::id())->first();
        //     $user_info = Auth::user()->id;
        //     if ($post_data) {
        //         $post = $post_data;
        //     } else {
        //         $post = (object) array("name" => null, "location" => null, "bio" => null, "image_post" => "no_image_post.png", "id" => null);
        //     }
        //     // dd($user_info);
        //     return view('post.index', compact('post', 'user_info'));
    }

    public function store(Request $request)
    {
        // dd($request->all());
        $request->validate([
            'user_info' => 'exists:users,id',
            'pesan' => 'required',
            'gambar' => 'required_without:profile_id|image|mimes:jpeg,png,jpg|max:2048',
            'caption' => 'required',
            'quote' => 'required',
        ]);
        $image_gambar = time() . "." . $request->gambar->extension();
        $request->gambar->move(public_path('images'), $image_gambar);

        post::create([
            "pesan" => $request["pesan"],
            "gambar" => $image_gambar,
            "caption" => $request["caption"],
            "quote" => $request["quote"],
            "user_id" => $request["user_info"],
        ]);


        //profil data from user_id
        $data_profile = Profile::all();
        $newCollection = $data_profile->mapWithKeys(function ($item) {
            return [
                $item['user_id'] => [
                    'id' => $item['id'],
                    'name' => $item['name'],
                    'image_profile' => $item['image_profile'],
                ]
            ];
        });
        $newCollection->all();

        $user_info = Auth::id();
        $post = 1;
        $all_post = Post::orderBy('id', 'DESC')->get();

        return view('pages.home', compact('user_info', 'post', 'all_post', 'newCollection'));
    }

    public function show($id)
    {
        // $post = DB::table('post')->where('id', $id)->first();
        // return view('post.post', compact('post'));
    }

    public function edit($id)
    {
        // $post = DB::table('post')->where('id', $id)->first();
        // return view('post.edit', compact('post'));
    }

    public function update(Request $request, $id)
    {
        // $request->validate([
        //     'location' => 'required',
        //     'bio' => 'required',
        // ]);
        // $query = DB::table('follow')
        //     ->where('id', $id)
        //     ->update([
        //         'location' => $request['location'],
        //         'bio' => $request['bio'],
        //     ]);

        // return redirect('/follow');
    }

    public function destroy($id)
    {
        // DB::table('follow')->where('id', $id)->delete();
        // return redirect('/follow');
    }
}
