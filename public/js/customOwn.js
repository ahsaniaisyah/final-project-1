$(function () {
    //show post modal when first loaded
    $("#postModal").modal('show');

    // layout.nav add search users form - start here
    $('#su').select2({
        placeholder: "Search Users",
        language: { inputTooShort: function () { return ''; } },
        minimumInputLength: 1,
        maximumInputLength: 10,
        maximumDisplayOptionsLength: 2,
        width: 'resolve',
        matcher: function (params, data) {
            // If there are no search terms, return all of the data
            if ($.trim(params.term) === '') {
                return data;
            }

            // `params.term` should be the term that is used for searching
            // `data.text` is the text that is displayed for the data object
            if (data.text.toLowerCase().startsWith(params.term.toLowerCase())) {
                var modifiedData = $.extend({}, data, true);
                modifiedData.text += '';

                // You can return modified objects from here
                // This includes matching the `children` how you want in nested data sets
                return modifiedData;
            }

            // Return `null` if the term should not be displayed
            return null;
        },
    });

    $("#su").on("select2:select", function (e) {
        window.open(e.params.data.id, '_self');
    });
    // layout.nav add search users form - end here

    //profile.show followed button for unfollow
    $('#followed_button').on('click', function () {
        return confirm('Unfollow User ?');
    });
    // following button success toast
    $('#following_button').on('click', function () {
        const Toast = Swal.mixin({
            toast: true,
            position: 'top-end',
            showConfirmButton: false,
            timer: 3000,
            timerProgressBar: true,
            didOpen: (toast) => {
                toast.addEventListener('mouseenter', Swal.stopTimer)
                toast.addEventListener('mouseleave', Swal.resumeTimer)
            }
        })

        Toast.fire({
            icon: 'success',
            title: 'Signed Following'
        })
    });
});